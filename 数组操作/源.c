
//作业内容
//创建一个整形数组，完成对数组的操作
//
//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。
//要求：自己设计以上函数的参数，返回值。
#define num 5
#include<stdio.h>
void init(int arr[])
{
	int i = 0;
	for(i=0;i<num;i++)
		arr[i] = 0;
}
void print(int arr[])
{
	int i = 0;
	for (i = 0; i < num; i++)
		printf("%d\t", arr[i]);
}
void reverse(int arr[])
{
	int *left = &arr[0];
	int *right = &arr[num - 1];
	int tmp = 0;
	while (left < right)
	{
		tmp = *left;
		*left = *right;
		*right = tmp;
		left+=1;
		right-=1;
	}
}
int main()
{
	int i = 0;
	int arr[num] = { 1,2,3,4,5 };// 5 4 3 2 1
	init(arr);
		for(i=0;i<num;i++)
		printf("%d\t", arr[i]);
		printf("\n");

	print(arr);
	printf("\n");
	reverse(arr);
	for (i = 0; i < num; i++)
		printf("%d\t", arr[i]);
	printf("\n");

	return 0;
}