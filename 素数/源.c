#define _CRT_SECURE_NO_WARNINGS 1

//实现一个函数，判断一个数是不是素数。
//利用上面实现的函数打印100到200之间的素数。
//一个大于1的自然数，除了1和它本身外，不能被其他自然数整除，

#include<stdio.h>
int shu(int i)
{
	int j = 0;
	for (j = i - 1; j > 1; j--)
	{
		if (0 == i % j)
			return 0;
	}
	return 1;
}
int main()
{
	int i = 100;
	for (; i <= 200; i++)
	{
		if (1 == shu(i))
			printf("%d\t", i);
	}
	return 0;
}