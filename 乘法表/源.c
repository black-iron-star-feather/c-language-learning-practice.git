#include<stdio.h>
int main()
{
	int i = 0;
	int j = 0;
	int n = 0;
	for (i = 1; i <= 9; i++)
	{
		n = i;
		for (j = 1; j <= 9; j++)
		{
			printf("%4d", n);//也可以直接i*j，
			n += i;			//但这样应该没有使用加法快
		}					//这种情况只需要到实际乘法表看看找找规律等
		putchar('\n');//使输出的形式和真正的乘法表结构相似
	}
	return 0;
}
