//将数组A中的内容和数组B中的内容进行交换。（数组一样大）
#define NUM 5
#include<stdio.h>
void swap(int arr1[], int arr2[])
{
	int tmp = 0;
	int i = 0;
	for (i = 0; i < NUM; i++)
	{
		tmp = arr1[i];
		arr1[i] = arr2[i];
		arr2[i] = tmp;
	}

}

int main()
{
	int arr1[NUM] = { 1,2,3,4,5 };
	int arr2[NUM] = { 6,7,8,9,10 };
	int i = 0;
	swap(arr1, arr2);

	for (i = 0; i < NUM; i++)
	{
		printf("%d\t", arr1[i]);		
	}
	printf("\n");
	for (i = 0; i < NUM; i++)
	{
		printf("%d\t", arr2[i]);
	}
	printf("\n");
	return 0;
}