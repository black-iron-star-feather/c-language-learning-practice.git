#include <stdio.h>
//对数组的越界访问编译器不会报错
//属于运行时错误
//栈区地址的分配是从高到低
//数组下标对应的地址由低到高
//本题是先创建了 i 
int main()
{
    int i = 0;
    int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
    /*for (i = 0; i <= 12; i++)
    {
        arr[i] = 0;
        printf("hello bit\n");
    }*/
    printf("%d", arr[20]);
    return 0;
}