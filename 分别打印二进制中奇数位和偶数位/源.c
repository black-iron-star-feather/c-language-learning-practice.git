#define _CRT_SECURE_NO_WARNINGS 1
//打印整数二进制的奇数位和偶数位
//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//就是把二进制的0 1 当做字符来打印
//比如说10 就是1010 打印 字符 1010
#include<stdio.h>
int main()
{
	int i = 0;
	int a = 3;
	char ch[32] = { '0' };
	scanf("%d", &a);
	while (a)
	{
		if (a % 2)
		{
			ch[i]='1';
		}
		else
		{
			ch[i]='0';
		}
		i++;
		a = a >> 1;
	}

	for (i = 31; i >=0; i--)
		printf("%c", ch[i]);
	return 0;
 }