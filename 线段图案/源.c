#include<stdio.h>
int main()                                 //多组输入和用减减的for循环
{
    int a = 0;                             //不懂先点点这里↓↓↓ （我的CSDN博客上有详解）
    while ((scanf("%d", &a) != EOF))       //https://blog.csdn.net/zoe23333/article/details/122072341?spm=1001.2014.3001.5501
    {                                      
        for (; a > 0; a--)                 //注意这里的 a - -
            printf("*");
        printf("\n");
    }
    return 0;
}
