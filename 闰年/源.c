#define _CRT_SECURE_NO_WARNINGS 1

//实现函数判断year是不是润年。
int year(int a)
{
	return (!(a%4)&&(a%100))||!(a%400);
}

#include<stdio.h>

int main()
{
	int a = 0;
	scanf("%d", &a);
	if (year(a))
		printf("%d是闰年。\n", a);
	else
		printf("%d不是闰年。\n", a);

	return 0;
}