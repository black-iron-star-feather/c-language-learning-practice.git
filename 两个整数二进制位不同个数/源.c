#define _CRT_SECURE_NO_WARNINGS 1
//输入两个整数，求两个整数二进制格式有多少个位不同
#include<stdio.h>
int main()
{
    int a = 0;
    int b = 0;
    int c = 0;
    int count = 0;
    scanf("%d%d", &a, &b);
    c = a ^ b;
    while (c)
    {
        if (c % 2)
            count++;
        c = c >> 1;
    }
    printf("%d", count);
    return 0;
}