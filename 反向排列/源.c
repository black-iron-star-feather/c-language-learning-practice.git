//
//字符串逆序（递归实现）
//编写一个函数 reverse_string(char* string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。
//比如 :char arr[] = "abcdef";
//逆序之后数组的内容变成：fedcba
//那么分两步来执行。第一步找出字符，第二步交换。
#include<stdio.h>
#include<string.h>
void reverse_string(char* string)
{
	char *left = string;
	char* right = string+strlen(string) - 1;//注意这里是指针变量，需要的赋值量是地址
	char tmp = *right;
	*right = '\0';
	if(strlen(string)>=2)
	reverse_string(string + 1);

	*right = *left;
	*left = tmp;

	//while (left < right)
	//{
	//	char tmp = *left;
	//	*left = *right;
	//	*right = tmp;
	//	然后分别在+  -//非递归
	//}

}

int main()
{
	int i = 0;
	char arr[] = "abcdef";
	reverse_string(arr);
	for (i = 0; i < 6; i++)
		printf("%c", arr[i]);
	return 0;
}
