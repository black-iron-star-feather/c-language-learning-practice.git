#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>					//这个代码是求A的值是B的百分之几
int main(void)						//所以要注意数据类型，这里用flaot比较好
{									//printf那里用%.0f指的是输出0位小数float类型的数据
	float a = 0, b = 0;
	puts("请输入两个整数。");
	printf("整数A：\n");
	scanf("%f", &a);
	printf("整数B：\n");
	scanf("%f", &b);
	printf("A的值是B的%.0f%%。\n", (a / b)*100);
	return 0;
}