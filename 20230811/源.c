#define _CRT_SECURE_NO_WARNINGS 1
//BC41 牛牛的球
//
//#include <stdio.h>
//
//int main() {
//    int r;
//    scanf("%d", &r);
//    double v = 3.14 * r * r * r;
//    v = (v * 4) / 3;
//    printf("%.2lf\n", v);
//    return 0;
//}
//BC42 小乐乐定闹钟
//#include <stdio.h>
//
//int main() {
//    int a, b, m;
//    scanf("%d:%d%d", &a, &b, &m);
//    int minute = (m + b) % 60;
//    int hour = ((m + b) / 60 + a) % 24; //别忘记只有24小时
//    printf("%02d:%02d", hour, minute);
//    return 0;
//}

//BC44 小乐乐与欧几里得
//好吧，这题我本来也是用下文的法一，但说我的程序运行时间过长。那么只能使用数学方法了。
//求最大公约数常用的有两种方法，一是九章算术中的更相减损术：大数减小数直到相等，
//相等的数即最大公约数，该算法时间复杂度约为O(N)；二是欧几里得的辗转相除法：大数除以小数取余数(相当于模运算)，
//直到余数为零时(也即模运算为零时)的除数(也即模数)就是最大公约数，该算法时间复杂度约为O(logN)。
//求最小公倍数的方法：原始数据的乘积除以最大公约数。
//#include <stdio.h>
//法一：无法通过，会出现算法复杂度过大的情况！--暴力求解不太行
/*
int main()
{
    long long n,m;
    scanf("%lld %lld",&n,&m);
    //求最大公约数
    long long max = n>m?m:n;//假设n和m的较小值为最大公约数
    while( 1 )
    {
        if( n%max==0 && m%max==0)
        {
            break;
        }
        max--;
    }//此时max里面记录的就是最大公约数
    //最小公倍数
    long long min = n>m?n:m; //假设n和m的较大值为最小公倍数
    while(1)
    {
        if( min%n==0 && min%m==0 )
        {
            break;
        }
        min++;
    }
    printf("%lld",min+max);
    return 0;
}
*/

//重新优化后的算法
//int main()
//{
//    long long n, m;
//    scanf("%lld %lld", &n, &m);
//    //求最大公约数
//    long long max = 0;
//    long long min = 0;
//    long long tmp = 0;
//    //先将n和m进行保存，防止下面使用辗转相除的方法影响n和m的值
//    long long a = n;
//    long long b = m;
//
//    //辗转相除法
//    while (tmp = n % m)
//    {
//        n = m;
//        m = tmp;
//    }
//    max = m;
//    //最小公倍数=n*m/max
//    min = a * b / max;
//    printf("%lld", min + max);
//    return 0;
//}
//BC46 KiKi算期末成绩
//#include <stdio.h>
//
//int main() {
//    int a, b, c, d;
//    scanf("%d%d%d%d", &a, &b, &c, &d);
//    double sum = a * 0.2 + b * 0.1 + c * 0.2 + d * 0.5;
//    printf("%.1lf", sum);
//
//    return 0;
//}
//BC48 牛牛的线段
//#include <stdio.h>
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    int c, d;
//    scanf("%d%d", &c, &d);
//    int num1 = (a - c) * (a - c);
//    int num2 = (b - d) * (b - d);
//    printf("%d", num1 + num2);
//    return 0;
//}
