#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include"game.h"
//三子棋游戏的相关函数定义如下
void menu()
{
	printf("********************\n");
	printf("****** 1.play ******\n");
	printf("****** 0.exit ******\n");
	printf("********************\n");
}

//初始化棋盘
void chessboard(char board[ROW][COL],int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
			board[i][j] = ' ';
	}
	
}

//打印棋盘
void print_chessboard(char board[ROW][COL],int row, int col)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{
		for (j = 0; j < col; j++)
		{
			//注意|比空格的数量少一
				printf(" %c ", board[i][j]);
				if (j < row - 1)
					printf("|");
		}
		printf("\n");
		//打印分割每一行的---
		if (i < row - 1)
		{
			for (j = 0; j < col; j++)
			{
				printf("---");
				if (j < col - 1)
					printf("|");
			}
		printf("\n");
		}
		
	}
	
}

//玩家下棋
void  player_move(char board[ROW][COL], int row, int col)
{
	int x = 0;
	int y = 0;
	
	while (1)
	{
		printf("请按顺序输入将要下棋的位置的行数和列数：\n");//得给个提示，告诉玩家可以开始下棋了
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			//没有棋子才可以下棋
			if (board[x - 1][y - 1] == ' ')
			{
				board[x - 1][y - 1] = '*';
				break;
			}
			else
			{
				printf("该坐标被占用，请重新输入\n");
			}
		}
		else
		{
			printf("坐标输入错误，请重新输入\n");
		}
	}
}

//电脑下棋
void  computer_move(char board[ROW][COL], int row, int col)
{	
	int x = 0;
	int y = 0;

	while (1)
	{
		 x = rand() % row;//row=3时x的结果范围: 0~2
		 y = rand() % col;

		 if ( x>=0 && x <= row && y >= 0 && y <= col && board[x][y] == ' ')
		 {
			 board[x][y] = '#';
			 break;
		 }

	}
}

//判断输赢 //这里思考下判断顺序，先判断谁是否赢了？没赢就看是否平局？没有平局才可以继续下棋

//判断棋盘是否满了，没赢就满是平局
static int if_full(char board[ROW][COL], int row, int col)
{
	//这个函数只为 is_win 服务，所以可以限制他的使用范围只在本文件中
	int i = 0;
	int j = 0;
	for (i = 0; i < row; i++)
	{		
		for (j = 0; j < col; j++)
		{
			if (board[i][j] == ' ')
			{
				return 0;//没满
			}
		}
	}
	return 1;//满了
}

char is_win(char board[ROW][COL], int row, int col)
{
	int i = 0;
	//判断行
	for (i = 0; i < row; i++)
	{
		if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][1] != ' ')
		{
			return board[i][1];
		}
	}

	//判断列
	for (i = 0; i < col; i++)
	{
		if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[1][i] != ' ')
		{
			return board[1][i];
		}
	}

	//对角线
	if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != ' ')
	{
		return board[1][1];
	}
	if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != ' ')
	{
		return board[1][1];
	}

	//这里的return是按我们分析的判断顺序执行的，
	//如果上面输赢判断出来了那么该函数会直接跳出去的
	 
	//判断平局
	if (if_full(board, row, col) == 1)
	{
		return 'Q';
	}

	//继续
	return 'C';
}

