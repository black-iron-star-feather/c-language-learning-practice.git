#define _CRT_SECURE_NO_WARNINGS 1
#define ROW 3
#define COL 3
#include<stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

//注意三子棋游戏实现的逻辑
//相关自定义函数的声明

//打印 功能选项单
void menu();
//初始化棋盘
void chessboard(char board[ROW][COL], int row, int col);
//打印棋盘 
void print_chessboard(char board[ROW][COL], int row, int col);

void game();

//玩家下棋（
void  player_move(char board[ROW][COL], int row, int col);

//判断输赢
char is_win(char board[ROW][COL], int row, int col);

//电脑下棋
void  computer_move(char board[ROW][COL], int row, int col);
		
