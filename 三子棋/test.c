#define _CRT_SECURE_NO_WARNINGS 1
#include"game.h"

//测试三子棋游戏
void test()
{
	int a = 0;
	do
	{
		menu();
		scanf("%d", &a);
		if (1 == a)
		{
			game();
			break;
		}
		else
		{
			printf("输入错误，请重新输入：\n");
		}
	} while (a);
	printf("退出游戏。\n");
}
void game()
{
	char ch = 0;
	//创建二维数组存放下棋数据
	char board[ROW][COL] = { 0 };
	//初始化棋盘
	chessboard(board, ROW, COL);
	//打印棋盘
	print_chessboard(board, ROW, COL);
	//随机数
	srand((unsigned int)time(NULL));
	
	while (1)
	{
		printf("  玩家下棋 \n");
		//玩家下棋（接着打印下棋结果）
		player_move(board, ROW, COL);
		print_chessboard(board, ROW, COL);

		//判断输赢		
		if ((ch = is_win(board, ROW, COL)) != 'C')
			break;

		//电脑下棋（接着打印下棋结果）
		printf("  电脑下棋 \n");
		Sleep(1000);//让玩家的有点感觉，不要太慌
		computer_move(board, ROW, COL);
		print_chessboard(board, ROW, COL);

		//判断输赢
		if ((ch = is_win(board, ROW, COL)) != 'C')
			break;
	}

	if ('*' == ch)
		printf("玩家赢。\n ");
	if ('#' == ch)
		printf("电脑赢。\n ");
	if ('Q' == ch)
		printf("平局。\n ");

}

int main()
{
	test();
	return 0;
}