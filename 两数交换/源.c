#define _CRT_SECURE_NO_WARNINGS 1

//实现一个函数来交换两个整数的内容。

#include<stdio.h>

void swap(int* pa, int* pb)
{
	int tmp = 0;

	tmp = *pa;
	*pa = *pb;
	*pb = tmp;

}
int main()
{
	int a = 2;
	int b = 9;
	printf("交换前：a=%d\tb=%d\n", a, b);
	swap(&a, &b);
	printf("交换后：a=%d\tb=%d\n", a, b);
	return 0;
}