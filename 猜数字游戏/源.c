#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void menu()
{
	printf("**********************\n");
	printf("***** 1.开始游戏 *****\n");
	printf("***** 2.退出游戏 *****\n");
	printf("**********************\n");
}

void game()
{
	int num = rand()%100+1;
	int n = 0;
	while (1)
	{
		printf("请输入数字：");
		scanf("%d", &n);
		if (n == num)
		{
			printf("猜对了！恭喜你。");
			break;
		}
		if (n > num)
			printf("猜大了 ");
		if (n < num)
			printf("猜小了 ");
	}

}

int main()
{
	srand((unsigned)time(NULL));
	int a = 0;
	while (1)
	{
		menu();
		scanf("%d", &a);
		if (1 == a)
		{
			game();
		}
		else if (2 == a)
			 break;
		printf("输入错误，请重新输入！\n");
	}

	return 0;
}