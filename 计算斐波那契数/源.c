#define _CRT_SECURE_NO_WARNINGS 1

//计算斐波那契数
//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1
#include<stdio.h>
//非递归
//int S(int a)
//{
//	int b = 1;
//	int c = 1;
//	int e = 1;
//	while (a>2)
//	{
//		e = b + c;
//		b = c;
//		c = e;
//		a--;
//	}
//	return e;
//}
//递归
int S(int a)
{
	if (a > 2)
		return S(a-1)+S(a-2);
	else
		return 1;

}
int main()
{
	int a = 0;
	scanf("%d", &a);
	printf("%d\n", S(a));
	return 0;
}