#define _CRT_SECURE_NO_WARNINGS 1
//不使用累计乘法的基础上，通过移位运算（ << ）实现2的n次方的计算。
//
//数据范围：0 \le n \le 31 \0≤n≤31
//输入描述：
//一行输入整数n（0 <= n < 31）。
//    输出描述：
//    输出对应的2的n次方的结果。
//    示例1
//    输入：
//    2
//    输出：
//    4

//也许左移一位扩大两倍

#include<stdio.h>
int main()
{
    int n = 0;
    scanf("%d", &n);
    printf("%d\n", 2 << (n - 1));
    return 0;
}