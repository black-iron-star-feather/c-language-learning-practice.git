#define _CRT_SECURE_NO_WARNINGS 1

//BC20 进制A+B
//
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%0x %o", &a, &b);
//    printf("%d\n", a + b);
//    return 0;
//}
//BC21 牛牛学加法
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    printf("%d\n", a + b);
//
//    return 0;
//}
//BC22 牛牛学除法
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    printf("%d\n", a / b);
//    return 0;
//}
//BC23 牛牛学取余
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    printf("%d\n", a % b);
//    return 0;
//}
//BC24 浮点数的个位数字
//
//#include <stdio.h>
//
//int main() {
//    double num;
//    scanf("%lf", &num);
//    printf("%d", (int)num % 10); //强转，截断。去掉小数部分
//    return 0;
//}
//BC25 牛牛买电影票
//#include <stdio.h>
//
//int main() {
//    int num;
//    scanf("%d", &num);
//    printf("%d\n", num * 100);
//    return 0;
//}
//BC26 计算带余除法
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    printf("%d %d", a / b, a % b);
//    return 0;
//}
//BC27 整数的个位
//
//#include <stdio.h>
//
//int main() {
//    int a;
//    scanf("%d", &a);
//    printf("%d", a % 10);
//    return 0;
//}
//BC28 整数的十位
//
//#include <stdio.h>
//
//int main() {
//    int a;
//    scanf("%d", &a);
//    printf("%d", (a / 10) % 10);
//    return 0;
//}
//BC29 开学？
//要0—多少，就%多少，余数包括在内，但无自己
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    int num = (a + b) % 7;
//    if (num == 0)
//    {
//        printf("%d", 7);
//    }
//    else
//    {
//        printf("%d", num);
//    }
//    return 0;
//}
//BC30 时间转换
//取模也可以理解为去掉比它大的
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d", &a);
//    printf("%d %d %d", a / 3600, (a % 3600) / 60, ((a % 3600) % 60));
//    return 0;
//}
//BC32 你能活多少秒
//
//#include <stdio.h>
////
//int main() {
//    int a=0;
//    scanf("%d", &a);
//    long num = a * 3156;
//    num *= 10;//很奇怪不能连续写。
//    num *= 10;
//    num *= 10;
//    num *= 10;
//    printf("%ld\n", num);
//    return 0;
//}
//
//int main()
//{
//    int age;
//    //一年有3.156×10的7次方秒 =3156×10的4次方秒
//    int time = 3156;//直接令时间为3156，就算人活500年也不会溢出
//
//    scanf("%d", &age);
//    time = age * time;
//    printf("%d0000", time);//计算的结果再补最后的四个0
//    return 0;
//}
//int main() {
//	    int a;
//	    scanf("%d", &a);
//	    long num = a * 3156*10*10*10*10;
//	    
//	    printf("%ld\n", num);
//	    return 0;
////	}
//BC31 2的n次方计算
//
//#include <stdio.h>
//
//int main() {
//    int a = 1, b = 2;
//    scanf("%d", &a);
//    b <<= (a - 1);
//    printf("%d", b);
//    return 0;
//}
//BC33 统计成绩
//
//#include <stdio.h>
//
//int main() {
//    int n;
//    scanf("%d", &n);
//    float score;
//    float max = 0.0;
//    float min = 100.0;
//    float sum = 0.0;
//    float avg = 0.0;
//    int count = 0;  //用这样的方法控制输入次数
//    while (scanf("%f", &score) != EOF)
//    {
//        count++;
//        if (count > n)
//        {
//            break;
//        }  
//        sum += score;
//        if (score >= max)
//        {
//            max = score;
//        }
//        if (score <= min)
//        {
//            min = score;
//        }
//    }
//    avg = sum / n;
//    printf("%.2f %.2f %.2f", max, min, avg);
//    return 0;
//}
//BC34 计算三角形的周长和面积
//求三角形面积用海伦公式，只需要知道三边
//#include <stdio.h>
//#include<math.h>
//int main() {
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    float circumference = a + b + c;
//    float d = circumference / 2.0;
//    float area = sqrt(d * (d - a) * (d - b) * (d - c));//sqrt开平方
//    printf("circumference=%.2f area=%.2f", circumference, area);
//    return 0;
//}
//BC35 KiKi和酸奶
//
//#include <stdio.h>
//
//int main() {
//    int a, b, c;
//    while (scanf("%d %d %d", &a, &b, &c) != EOF) { // 注意 while 处理多个 case
//        // 64 位输出请用 printf("%lld") to 
//        int num = (a * b - c) / b;
//        printf("%d\n", num);
//    }
//    return 0;
//}
//BC36 温度转换
//#include <stdio.h>
//int main() {
//    double a;
//    scanf("%lf", &a);
//    printf("%lf", 5.0 / 9.0 * (a - 32));
//    return 0;
//}
//BC37 牛牛的圆
//#include <stdio.h>
//int main() {
//    int r;
//    scanf("%d", &r);
//    printf("%.2f", 3.14 * r * r);
//    return 0;
//}
//BC38 牛牛的并联电路
//#include <stdio.h>
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    float zhi = 1.0 / a + 1.0 / b;
//    zhi = 1.0 / zhi;
//    printf("%2f", zhi);
//    return 0;
//}
//BC39 牛牛的水杯
//
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    float v = 3.14 * a * b * b;
//    int n = 10000 / (int)v + 1; //多出来的算一杯但除法是直接抹去所以要加
//    printf("%d", n);
//    return 0;
//}
//BC40 牛牛的等差数列
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d%d", &a, &b);
//    printf("%d", b + (b - a));
//    return 0;
//}