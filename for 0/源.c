#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int i = 0;
	scanf("%d", &i);
	int min = i;
	int max = i;//注意这里先假设，千万不要赋值0，否则min就为0了。
	for (;;)	//这里可以为空语句，只要循环体内有结束循环的条件就好。
	{
		scanf("%d", &i);
		if (i == -1)
			break;
		min = min < i ? min : i;
		max = max > i ? max : i;
	}
	printf("最高成绩为 %d", max);
	printf("最低成绩为 %d", min);
	return 0;
}
//3、编程实现：从键盘上输入某小组同学考试成绩（正整数），求其中最高成绩和最低成绩。
//输入的成绩以 - 1结束。 