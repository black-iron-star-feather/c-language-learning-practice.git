#define _CRT_SECURE_NO_WARNINGS 1
//递归实现n的k次方
//
//作业内容
//编写一个函数实现n的k次方，使用递归实现。
#include<stdio.h>
int KCF(int n, int k)
{
	int num = 1;
	if (k > 0)
	{
		return n * KCF(n, --k);
	}
	else
		return 1;
}

int main()
{
	int n = 0;
	int k = 0;
	printf("请按顺序输入n和其的k次方的数值：\n");
	scanf("%d%d", &n, &k);
	printf("n的k次方 = %d\n", KCF(n, k));
	return 0;
}