#define _CRT_SECURE_NO_WARNINGS 1
//实现一个 函数，可以左旋字符串中的k个字符。
//
//例如：
//
//ABCD左旋一个字符得到BCDA
//
//A B C D左旋两个字符得到CD AB //
//P+K的提前，K的放到后面。怎么放？
//arr[0-k]=arr[p+k];??
//
#include<stdio.h>
void change(char arr[], int k)
{
	int i = 0;
	char tmp = '0';
	char* p = arr + k;
	for (i = 0; *p != '\0'; i++)
	{
		p++;
	}
	//p--;
	for (i = 0; k > 0; k--)
	{
		tmp = arr[i];
		arr[i]=*(p-k);
		*(p - k) = tmp;
		i++;
	}
	//这不是交换，交换的话顺序就直接乱了。注意交换是位置
	//按照k分成两部分。有接着的意味
	//把P+K后面的提前。怎么搞？按顺序赋值？写一个新的数组接收？ 拷贝还原？
}
int main()
{
	char arr[] = "abcd";
	int k = 0;
	scanf("%d", &k);
	change(arr, k);
	char* p = arr;
	while (*p != '\0')
	{
		printf("%c", *p);
		p++;
	}
	return 0;
}