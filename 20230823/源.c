#define _CRT_SECURE_NO_WARNINGS 1
BC24 浮点数的个位数字
#include <stdio.h>

int main() {
    double num;
    scanf("%lf", &num);
    printf("%d", (int)num % 10);
    return 0;
}