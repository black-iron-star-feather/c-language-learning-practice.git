#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int jx(int a);
int main(void)
{						//求从1到n的阶乘的和
	int n=0, sum = 0,i=0;
	puts("请输入n的值");
	scanf("%d", &n);
	for (i=n; i >0; i--)
		sum += jx(i);		//这里将i作为参数传到函数jx中，
	printf("从1到%d的阶乘的和为%d\n", n, sum);			//也可以想为在这里执行了函数jx并把函数中的a换成了i。
	
	return 0;
}
int jx (int a)		//求n的阶乘
{
	int  j = 0,n=1;
	for (j = 1; j < a+ 1; j++)
	 n*= j;
	return n;
}