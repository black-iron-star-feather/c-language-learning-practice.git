#define _CRT_SECURE_NO_WARNINGS 1
//BC3 牛牛学说话之-整数
//输入一个整数，输出这个整数
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("%d", &a);
//    printf("%d", a);
//    return 0;
//}

//BC4 牛牛学说话之-浮点数
//#include <stdio.h>
//
//int main() {
//    float a, b;
//    scanf("%f", &a);
//    printf("%.3f", a);
//    return 0;
//} 注意，什么数据使用什么数据类型，%.3表示小数点后面3位，如果没有.表示右对齐，负号左对齐

//BC6 牛牛的第二个整数
#include <stdio.h>
//
//int main() {
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    printf("%d\n", b);
//    return 0;
//}

//BC7 牛牛的字符矩形
//#include <stdio.h>
//
//int main() {
//    char ch;
//    scanf("%c", &ch);
//    for (int i = 1; i <= 3; i++)
//    {
//        for (int j = 1; j <= 3; j++)
//        {
//            printf("%c", ch);
//        }
//        printf("\n");
//    }
//    return 0;
//}

