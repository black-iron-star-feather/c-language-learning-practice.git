#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int max3(int a, int b, int c)
{
    int max = 0;
    if (a > b)
        max = a > c ? a : c;
    else max = b > c ? b : c;
    return (max);
}
int main()
{
    float m = 0;
    int a = 0, b = 0, c = 0;
    scanf("%d%d%d", &a, &b, &c);//除了这样更简便的方法是*1.0或直接int.0/ 或 /int.0
    m = (float)max3(a + b, b, c) / (float)(max3(a, b + c, c) + max3(a, b, b + c));
    printf("%.2f", m);
    return 0;
}
//牛客网 BC129 小乐乐计算函数
//其中 max3函数为计算三个数的最大值，如： max3(1, 2, 3) 返回结果为3。
//输入描述：
//一行，输入三个整数，用空格隔开，分别表示a, b, c。
//输出描述：
//一行，一个浮点数，小数点保留2位，为计算后m的值。
//示例1
//输入：
//1 2 3
//复制
//输出：
//0.30
