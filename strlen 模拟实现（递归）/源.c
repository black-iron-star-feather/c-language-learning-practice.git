#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现strlen
#include<stdio.h>
//int Str(char *str)//递归
//{
//	if (*str != '\0')
//		return 1 + Str(str+1);
//	else
//		return 0;
//}

int Str(char* str)//非递归
{
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str ++;
	}
	return count;
}

int main()
{
	char ch[] = "abc";
	printf("%d", Str(ch));
	return 0;
}