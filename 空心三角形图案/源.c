#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
void TR(int row)
{
    //先把开头和结尾处理了吧，既然明确是三角形的话
    printf("* \n");
    printf("* * \n");
    //中间只打印首尾
    for (int i = 3; i < row; i++)
    {
        printf("* ");
        for (int j = 1; j <= i; j++)
        {
            if (j == i)
            {
                printf("* \n");//注意打印空格才可以把*挤过去
            }
            else
                printf(" ");
        }
    }
    //最后一行
    for (int n = 0; n < row; n++)
    {
        printf("* ");
    }
    printf("\n");
}
int main()
{
    int row = 0;
   while (scanf("%d", &row) != EOF)
   {
       TR(row);
    }
    return 0;
}
//多组输入。。。。
