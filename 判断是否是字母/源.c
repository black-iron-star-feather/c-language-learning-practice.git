#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//65-90
//97-122

int main()
{
    char n = 0;//注意这里输入一个字符，才能让编译器自己转换为其ascll码来判断，所以是直接输入字符
    scanf("%c", &n);
    if ((65 <= n) && (n <= 90) || (97 <= n) && (n <= 122))
        printf("YES");
    else
        printf("NO");
    return 0;
}