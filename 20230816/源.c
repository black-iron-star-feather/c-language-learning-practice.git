#define _CRT_SECURE_NO_WARNINGS 1
//BC79 小乐乐求和
//#include <stdio.h>
//
//int main() {
//    long n;
//    scanf("%ld", &n);
//    long sum = 0;
//    while (n)
//    {
//        sum += n;
//        n--;
//    }
//    printf("%ld\n", sum);
//    return 0;
//}
//BC80 奇偶统计
//#include <stdio.h>
//
//int main() {
//    int n;
//    scanf("%d", &n);
//    int a = 0;
//    int b = 0;
//    while (n)
//    {
//        if (n % 2 == 0)
//        {
//            b++;
//        }
//        else {
//            a++;
//        }
//        n--;
//    }
//    printf("%d %d\n", a, b);
//    return 0;
//}
//BC81 KiKi求质数个数
//#include <stdio.h>
//int fun(int n)
//{
//    for (int i = 2; i < n; i++)
//    {
//        if (n % i == 0)
//        {
//            return 0;
//        }
//    }
//    return n;
//}
//
//int main() {
//
//    int count = 0;
//    for (int i = 101; i <= 999; i += 2)
//    {
//        if (fun(i) != 0) //是质数
//        {
//            count++;
//        }
//
//    }
//    printf("%d\n", count);
//    return 0;
//}
//BC82 乘法表
//#include <stdio.h>
//
//int main() {
//    int i = 1;
//    int j = 1;
//
//    for (i = 1; i <= 9; i++)
//    {
//        for (j = 1; j <= i; j++)
//        {
//            printf("%d*%d=%2d ", j, i, i * j);
//        }
//        printf("\n");
//    }
//    return 0;
//}
//BC83 牛牛学数列
//#include <stdio.h>
//
//int main() {
//    int n;
//    scanf("%d", &n);
//    int sum = 0;
//    for (int i = 1; i <= n; i += 2) //用n判断，因为数列只可能是正数项
//    {
//
//        sum += i;
//    }
//    for (int i = -2; i <= n; i -= 2)
//    {
//        if (i > n || i < (-n))
//        {
//            break;
//        }
//        sum += i;
//    }
//    printf("%d\n", sum);
//    return 0;
//}