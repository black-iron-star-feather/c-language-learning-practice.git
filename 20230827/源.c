#define _CRT_SECURE_NO_WARNINGS 1
BC102 带空格直角三角形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF)
    {
        // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to      
        for (int i = 1; i <= a; i++)
        {
            for (int j = a; j > i; j--)
            {
                printf("  ");

            }
            for (int j = 1; j <= i; j++)
            {
                printf("* ");

            }
            printf("\n");
        }

    }
    return 0;
}
