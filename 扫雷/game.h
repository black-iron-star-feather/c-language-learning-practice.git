#pragma once
//等会先有道哈意思，搜哈为什么出现在这里，有什么作用
#define _CRT_SECURE_NO_WARNINGS 1
#define ROW 9
#define COL 9

#define ROWS ROW+2
#define COLS COL+2

#define MINE 10

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
//初始化棋盘
void init_board(char arr[ROWS][COLS], int row, int col, char ret);

//打印看看
void show_board(char arr[ROWS][COLS], int row, int col);

//埋雷
void set_mine(char arr[ROWS][COLS], int row, int col);


//排查雷
void find_mine(char mine[ROWS][COLS], char show[ROWS][COLS],int row, int col);

	//判断游戏结束