#include "game.h"

//初始化棋盘
void init_board(char arr[ROWS][COLS], int rows, int cols, char ret)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < rows; i++)
	{
		for (j = 0; j < cols; j++)
		{
			arr[i][j] = ret;
		}
	}
}

//打印看看
void show_board(char arr[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	printf("-----------扫雷-----------\n");

	for (i = 0; i <= col; i++)//灵活打印列标
	{
		printf("%d ", i);
	}
	printf("\n");

	for (i = 1; i <= row; i++)
	{
		printf("%d ", i);
		for (j = 1; j <= col; j++)
		{
			printf("%c ",arr[i][j]);
		}
		printf("\n");
	}
	printf("-----------扫雷-----------\n");
}

//埋雷
void set_mine(char arr[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int count = MINE;
	while (count)
	{
		x = rand() % row + 1; //1~9
		y = rand() % row + 1;

		if ('0' == arr[x][y])
		{
			arr[x][y] = '1'; //'1'表示雷
			count--;
		}
	}
}

//得出排查后雷的信息
int get_mine_count(char arr[ROWS][COLS], int row, int col)
{
	int i = 0;
	int j = 0;
	int count = 0;
	for (i = -1; i <= 1; i++)
	{
		for (j = -1; j <= 1; j++)
		{			
			if ('1' == arr[row + i][col + j])
				count++;
		}
	}

	return count;
}

//排查雷
void find_mine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	
	int x = 0;
	int y = 0;
	int win = 0;
	while (win< row * col - MINE)
	{
		printf("请按行列顺序输入要排查的坐标：\n");
		scanf("%d%d", &x, &y);
		if (x >= 1 && x <= 9 && y >= 1 && y <= 9)
		{
			if ('1' == mine[x][y])
			{
				printf("是雷！被炸了！！！\n");
				show_board(mine, ROW, COL);				
				break;
			}
			else
			{
				int count = get_mine_count(mine, x, y); //函数返回值是 int 类型
				show[x][y] = count + '0';			//数组存储的是 char 类型
				show_board(show, ROW, COL);
				win++;
			}
		}
		else
			printf("坐标非法，请重新输入：");
	}
	if (win == row * col - MINE)
	{
		printf("恭喜你，排雷成功\n");
		show_board(mine, ROW, COL);
	}

}
	//判断游戏结束