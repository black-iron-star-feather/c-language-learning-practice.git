#include"game.h"

void menu()
{
	printf("***********************\n");
	printf("*******  1.play  ******\n");
	printf("*******  0.exit  ******\n");
	printf("***********************\n");
}
void game()
{
	srand((unsigned int)time(NULL));
	//初始化二维数组
	char mine[ROWS][COLS] = { 0 };
	char show[ROWS][COLS] = { 0 };
	//使用的是11*11，调用的是其中的9*9
	
	//初始化棋盘
	init_board(mine, ROWS, COLS, '0');
	init_board(show, ROWS, COLS, '*');

	//打印看看

	//show_board(mine, ROW, COL);
	//注意雷是不打印给玩家看的，
	//但我们写的时候可以打印出来看看是否写对了。

	show_board(show, ROW, COL);

	//埋雷
	set_mine(mine, ROW, COL);
	//show_board(mine, ROW, COL);

	//排查雷
	find_mine(mine, show, ROW, COL);

}
void test()
{
	int i = 0;
	do
	{
		menu();
		scanf("%d", &i);
		if (1 == i)
		{
			printf("开始游戏 >\n"); //好的游戏体验感，
			game();					//给个提示，不会太突然
			break;
		}
		else if(i!=0)  //这里注意美观
			printf("输入错误，请重新输入：\n");

	} while (i);
	printf("退出游戏。\n");
}

int main()
{

	test();

	return 0;
}