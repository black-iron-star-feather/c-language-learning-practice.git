#define _CRT_SECURE_NO_WARNINGS 1
//
//计算一个数的每位之和（递归实现）
//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19
#include<stdio.h>
int DigitSum(int n)
{
	int a = 0;
	a = a + (n % 10);
	if(n>0)
		return a + DigitSum(n / 10);
	 //记得要把每一步返回的值加起来所以直接这样
	//这里需要这个变量a

}

int main() 
{
	int a = 0;
	scanf("%d", &a);
	printf("%d\n",DigitSum(a));
	return 0;
}