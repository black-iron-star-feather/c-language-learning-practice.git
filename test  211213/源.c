#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
	int a = 0;
	int b = 0;
	int max = 0;
	scanf("%d%d", &a, &b);	
	max = a > b ? a:b;	//唯一一个三目操作符，又叫条件操作符。
	printf("%d", max);	//exp1？exp2：exp3  若exp1（表达式）为真（成立），则整个操作符返回exp2的值；
						//若exp1（表达式）为假（不成立），则整个操作符返回exp3的值。
	return 0;
}