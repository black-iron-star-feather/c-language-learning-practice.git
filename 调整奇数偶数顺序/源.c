#define _CRT_SECURE_NO_WARNINGS 1
//调整数组使奇数全部都位于偶数前面。
//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。

//找到第一个偶数和下一个 奇数 ，然后把它们交换位置即可
//这样就保证了 前面都是奇数，后面都是偶数这个条件
//实话说，这样也麻烦了点，如果前面好几个偶数连着，那么被找过的位置就要循环多次，
//记住了

#include<stdio.h>

void PX(int *p, int i)
{
	int* left = p;//这里的数是要成为交换数
	int* right = p + i-1;
	while (left < right)
	{
		while ((left < right) && (*left % 2==1))	//左边找偶数要换到右边
		{
			//奇数，找到偶数就结束
			//这里是负责找到这个偶数，那么找到了这个偶数才结束这个循环
			//所以循环体内就只是++直到找到偶数了就出去
			left++;
		}
		while ((left < right) && (*right % 2==0))
		{
			//偶数找到奇数就结束
			right--;
		}
		//循环相关数在循环内改变就要再判断
		if (left < right)
		{
			int tmp = *left;
			*left = *right;
			*right = tmp;
		}
	}
}
////这个代码不行，只找了一遍那
//void PX(int *p, int i)
//{
//	int n = 0;
//	int ret = 0;
//	int tmp = 0;
//	for (n = 0; n < i; n++)
//	{
//		if (!((*(p + n)) % 2))//第一个偶数
//		{
//			ret = n;
//			//注意，要找到了第一个偶数才开始计算下一个奇数
//			//所以条件要包含在内，“启动”
//			int a = n;
//			while (1)//接着的奇数
//			{
//				if (*(p + a+1) % 2 == 1)
//				{
//					tmp = *(p + a);
//					*(p + a) = *(p + a + 1);
//					*(p + a + 1) = tmp;
//					break; //能够发生这个交换后才跳出
//				}
//				a++;//找不到就在往后找
//				if (a == i + 1)
//				{
//					break;//找完所有数字也跳出
//				}
//			}
//	
//		}
//	}
//}

int main()
{
	int sz = 0;
	int n = 0;
	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
	sz = sizeof(arr) / sizeof(arr[0]);
	PX(arr,sz);
	for (n = 0; n < sz; n++)
	{
		printf("%d", arr[n]);
	}
	return 0;
}