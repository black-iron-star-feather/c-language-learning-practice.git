#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include<string.h>
//悬空else，不过，这应该算是一条if语句。
//int main()
//{
//	int a = 0;
//	int b = 2;
//	if (a == 0)
//		if (b == 3)
//			printf("hehe\n");
//		else
//			printf("haha\n");
//	return 0;
//}

// 判断一个数是否为奇数,判断正误，暗含两个选择。
//int main()
//{
//	int num = 0;
//	scanf("%d", &num);
//	if (num % 2) 
//	{
//		printf("奇数");
//	}
//	return 0;
//}

//输出1-100之间的奇数
//int main()
//{
//	int num = 1;
//	while (num < 100)
//	{
//		printf("%d\n", num);
//		num += 2;
//	}
//	return 0;
//}

//计算n的阶乘 和 1到10的阶乘的和
//int test(int n)
//{
//	int i = 0;
//	int sum = n;
//	for (i = 1; i < n; i++)
//	{
//		sum *= i; //这里不能写n，因为n是判断条件。
//	}
//	return sum;
//}
//
//int main()
//{
//	int n = 3;
//	printf("%d\n",test(n));
//	int i = 0;
//	int sum = 0;
//	for (i = 1; i <= 10; i++)
//	{
//		sum += test(i);
//	}
//	printf("%d\n", sum);
//
//	return 0;
//}

//在一个有序数组中查找具体的某个数字n。（讲解二分查找）
//int main()
//{
//	int arr[6] = { 1,2,3,4,5,6 };
//	int i = 0;
//	int left = 0;
//	int right = sizeof(arr)/4 - 1;
//	int mid = (left + right) / 2;
//	int input = 4;
//	while (left <= right)
//	{
////		if (input == arr[mid])
//		{
//			printf("%d\n", mid);
//			break;
//		}
//
//		if (arr[mid] < input)
//		{
//			left = mid;
//		}
//		else
//		{
//			right = mid;
//		}
//
//		mid = (left + right) / 2;
//
//	}
//	return 0;
//}