#define _CRT_SECURE_NO_WARNINGS 1
//打印水仙花数
//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，
//其各位数字的n 次方 之和确好等于该数本身，
//如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”
#include<stdio.h>
//void GET(int* p, long long a)
//{
//	int i = 0;
//	for (i = 0; i < )
//
//		a % 10 * ret
//		a = a / 10
//		a % 10 * ret
//		a = a / 10
//		a % 10 * ret
//
//		if ((a / 10) != 0)
//			sz++;
// 
// for(i=0; i<sz; i++)
//	{
//		sum = sum+(a % 10 )* sz
//		a = a / 10
// 
//		}
//
//}
//求次方
int CF(int a, int sz)
{
	sz--;
	int ret = a;
	while (sz)
	{	
		ret=ret*a;	//记住这里如果a*a那么这两个a的值都在增大
		sz--;		//要把a增大的值乘原来的a值才是次方乘
	}
	return ret;
}

int main()
{
	int a = 0;
	int b = 0;
	int   i = 0;
	int sum = 0;
	int  sz = 1;

	for (b = 0; b < 100000; b++)
	{
		a = b;
		while (a /= 10)  //这个必须当做判断条件写，如果a是判断条件，那么a=2会进入循环是sz加1.
		{			
			sz++;
		}
		a = b;
		sum = 0;
		for (i = 0; i < sz; i++)
		//while(a)
		{
			sum = sum + CF( a%10, sz);
			a = a / 10;			 
		}
		sz = 1;
		if (sum == b)
			printf("%d\t", b);
	}
	return 0;
}