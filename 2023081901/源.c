#define _CRT_SECURE_NO_WARNINGS 1
BC90 小乐乐算多少人被请家长
#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    int i = 0;
    int a = 0;
    int b = 0;
    int c = 0;
    int count = 0;
    int sum = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d%d%d", &a, &b, &c);
        sum = a + b + c;
        if (sum < (60 * 3))
        {
            count++;
        }

    }
    printf("%d", count);
    return 0;
}
BC119 最高分与最低分之差
#include <stdio.h>

int main() {
    int n;
    scanf("%d", &n);
    int max = 0;
    int min = 10000;
    int i = 0;
    int a = 0;
    int c = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d", &a);
        if (max < a)
        {
            max = a;
        }

        if (min > a)
        {
            min = a;
        }

    }
    c = max - min;
    printf("%d", c);
    return 0;
}
BC118 N个数之和
#include <stdio.h>

int main() {
    int a, b;
    scanf("%d", &a);
    int i = 0;
    int sum = 0;
    for (i = 0; i < a; i++)
    {
        scanf("%d", &b);
        sum = sum + b;
    }
    printf("%d", sum);
    return 0;
}
BC126 小乐乐查找数字
#include <stdio.h>

int main() {
    int n, x;
    scanf("%d", &n);
    int i = 0;
    int arr[100] = { 0 };
    int count = 0;
    for (i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }
    scanf("%d", &x);
    for (i = 0; i < n; i++)
    {
        if (x == arr[i])
        {
            count++;
        }
    }
    printf("%d", count);
    return 0;
}
