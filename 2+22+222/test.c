#define _CRT_SECURE_NO_WARNINGS 1
//求Sn=a+aa+aaa+aaaa+aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222
#include<stdio.h>
int main()
{
	int a = 0;
	int i = 0;
	int sum = 0;
	scanf("%d", &a);
	int b = a;
	for (i = 0; i < 5; i++)
	{
		sum += b;
		b = b * 10 + a;
	}
	printf("%d\n", sum);
	return 0;
}

//int main()
//{
//	int a = 0;
//	int sum = 0;
//	scanf("%d", &a);
//	sum = a * 5 + a * 40 + a * 300 + a * 2000 + a * 10000 ;
//	printf("%d\n", sum);
//	return 0;
//}