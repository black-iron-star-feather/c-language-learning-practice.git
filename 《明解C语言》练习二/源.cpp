#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>					//这个代码要注意double类型在对应的scanf函数的输入对应的是“%lf”
int main(void)
{
	double i;
	printf("请输入一个实数：");
	scanf("%lf", &i);
	printf("你输入的是%f", i);
	return 0;
}