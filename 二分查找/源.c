#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>
int main()
{
	int a[7] = { 2,3,4,5,6,7,8 };
	int right = sizeof(a)/sizeof(a[0])-1;
	int left = 0;
	int mid = 0;

	while ( left<=right )
	{
		mid = left + (right - left) / 2;
		if (a[mid] == 7)
		{
			printf("下标是 %d\n", mid);
			break;
		}
		if (a[mid] < 7)
		{
			left = mid;
		}
		if (a[mid] > 7)
		{
			right = mid;
		}
	}
	if (left > right)
		printf("没有找到，抱歉。");

	return 0;
}