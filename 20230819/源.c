#define _CRT_SECURE_NO_WARNINGS 1
BC113 数字三角形
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 

        int i = 0;
        int j = 0;

        for (i = 1; i <= a; i++)
        {
            for (j = 1; j <= i; j++)
            {
                printf("%d ", j);
            }
            printf("\n");

        }

    }
    return 0;
}