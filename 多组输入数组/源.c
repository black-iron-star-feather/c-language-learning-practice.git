#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
int Scan(int arr[7])//输入一组成绩
{
    int i = 0;
    //for(i=0; i<7; i++)
    //{
       // scanf("%d",&arr[i]);
    //}
    //注意判断这个循环结束的条件
    //既然只能利用scanf，不能多的读取，那就判断是否读取了7个数
    //若一次读取了7个数，也就是这一组，那么就打印，否则结束
    //在这里一次判断一个数就可以实现之前的多组输入了
    while ((scanf("%d", &arr[i])) != EOF)
    {
        i++;
        if (i == 7)
            break;
    }
    return i;
}

float Print(int arr[7])//返回平均成绩
{
    int i = 0;
    int sum = arr[0];
    int max = arr[0];
    int min = arr[0];
    //int tmp=0;不用交换，只要找出值最后减去就行

    for (i = 1; i < 7; i++)//得把最大最小值先找出来
    {
        sum += arr[i];
        if (arr[i] > max)
        {
            max = arr[i];
        }
        if (arr[i] < min)
        {
            min = arr[i];
        }
    }
    sum -= (max + min);
    return sum / 5.0;
}

int main()
{
    int arr[7] = { 0 };
    char ch = 0;
    while (Scan(arr) == 7)
    {
        printf("%.2f\n", Print(arr));
    }

    return 0;
}