#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>                
int main()                          //描述将一个四位数，反向输出。
{                                   //输入描述：
    int i = 0;                      //一行，输入一个整数n（1000 <= n <= 9999）。
    int a = 0;                      //输出描述：
    int b = 0;                      //针对每组输入，反向输出对应四位数。
    int c = 0;                      //示例1
    int d = 0;                      //输入：
    scanf("%d", &i);                //1234
    a = i / 1000;                         //输出：    
    b = (i / 100) % 10;                   //4321
    c = (i % 100) / 10;                     
    d = i % 10;
    printf("%d%d%d%d\n", d, c, b, a);
    return 0;
}
