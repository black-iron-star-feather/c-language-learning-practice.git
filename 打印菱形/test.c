#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//本题的关键，也就是*和‘ ’是随什么而变的？
//仔细观察，应该是行数，每行按规律增加。
//如果纵向观察，看列数，那么会发现我们写的时候
//增加的数值不好看 
//而且结合printf函数是每行每行打印的特点，我们应该尽量按行规律来写
//那么本题直接可以写了
//只需要用行来控制打印个数，不应该使用数组
//数组不灵活而且打印输出这里不好写
int main()
{
	int i = 0;
	int j = 0;
	int n = 0;
	scanf("%d", &n);//打印上半部分
	for (i = 1; i <=n; i++)// i是行数
	{
		for (j = n - i; j > 0; j--)
		{
			printf(" ");
		}
		for (j = 1; j <= i*2-1; j++)
		{
			printf("*");
		}
		printf("\n");
	}

	for (i = n - 1; i > 0; i--)// 下半部分 i是行数
	{
		for (j = n - i; j > 0; j--)
		{
			printf(" ");
		}
		for (j = 1; j <= i * 2 - 1; j++)
		{
			printf("*");
		}
		printf("\n");
	}

	return 0;
}

//int main()
//{
//	int i = 0;
//	int j = 0;
//	for (j = 0; j < 7; j++)
//	{
//		for (i = 0; i < 6; i += 2)
//		{
//			printf("%c ", arr[i]);			
//		}
//		int ret = j - i + 2;
//		for (; ret > 1; ret--)
//			printf("%c", ch[i]);
//
//		printf("\n");
//	}
//	return 0;
//}