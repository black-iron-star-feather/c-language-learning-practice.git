#define _CRT_SECURE_NO_WARNINGS 1
//递归和非递归分别实现求n的阶乘（不考虑溢出的问题）
#include<stdio.h>
//int CX(int n)//非递归
//{
//	int cx = 0;
//	for (cx = 1; n>1; n--)
//		cx *=n;
//	return cx;
//}
int CX(int n)//递归
{
	if (n > 1)
		return n * CX(n - 1);
	else
		return 1;
}
int main()
{
	int n = 0;
	scanf("%d", &n);
	printf("%d\n", CX(n) );

	return 0;
}