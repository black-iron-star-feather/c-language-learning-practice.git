#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int X(int y);
int main()
{
	int i = 1000;
	int a = 0;
	for (; i > 0; i--)
	{
		if (X(i) != 0)
		{
			printf(" %d  its factors are ", i);
			for (a = i - 1; a > 0; a--)
			{
				if (i % a == 0)
					printf(" %d、", a);
			}
			printf("\b\b。\n");
		}

	}
	return 0;
}
int X(int y)//判断y是不是完数
{				//注意y=1不用考虑
	int q = 0; //完数定义：某自然数除它本身以外的所有因子
	int w = 0;//之和等于该数，则该数被称为完数。
	int e = 0;
	for (q = y - 1; q > 0; q--)
	{
		if (y % q == 0)
			w += q;

	}
	if (w == y)
		return y;
	else return 0;
}
//4、一个数如果恰好等于它的因子之和，这个数就称为“完数”。
//例如，6的因子为1、2、3，而6＝1＋2＋3，因此6是“完数”。
//编程序找出1000之内的所有完数
//，并按下列格式输出其因子。6 its factors are 1、2、3。 