#define _CRT_SECURE_NO_WARNINGS 1

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。

#include<stdio.h>

void CF(int h)
{
	int a = 0;
	int b = 0;
	for (a=1;a<=h;a++)
	{
		for (b = 1; b <= a; b++)
		{
			printf("%d * %d = %4d\t", a, b,a * b);
		}
		printf("\n");
	}

}

int main()
{
	int h = 0;
	int l = 0;
	scanf("%d", &h);
	CF(h);

	return 0;
}