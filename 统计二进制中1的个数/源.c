#define _CRT_SECURE_NO_WARNINGS 1
//写一个函数返回参数二进制中 1 的个数。
//比如： 15    0000 1111    4 个 1
#include<stdio.h>
//int NumberOf1(int n)
//{
//    int count = 0;
//    while (n)
//    {
//        if (n % 2)
//            count++;
//        n = n >> 1;
//        if (count == 32)
//            break;
//    }// write code here
//    return count;
//}//-1有32个1，而编译器是算术右移
int NumberOf1(int n)
{
    int count = 0;
    int i = 0;
      for(i=0;i<32;i++)
        {
          if ((n >> i) % 2)
              count++;
           /* if (1==((n >> i) & 1))
                count++;*/
          //这两种都可以
        }
        return count;
        //不要移动n，因为如果n为负数，算术右移后会补1，
        //这样1的个数就不是它原来的了（变多了）
        // 但在这里，n移动i为后下一次n还是没变，因为没有写n>>=i
        // 有时候也许移动1（正整数嘛没问题）就会方便些也许    

}
int main()
{
    int n= 15;//算术右移补原符号位那！！，所以
    printf("%d",NumberOf1(n));
    //printf("%d", n);
	return 0;
}
//10000000000000000000000000000011
//11111111111111111111111111111100
//11111111111111111111111111111101  -3


