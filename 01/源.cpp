#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
/* 第一个代码 Hello World!
int main()
{
	printf("Hello World!");

	return 0;
}
*/

/*
	变量的定义和使用
*/
/*
int main()
{
	int a = 3;
	float num = 23.4;
	scanf("%f", &num);
	printf("%d  %.5f\n",a, num);
	return 0;
}
*/

//常量的定义和使用
/*
int main()
{
	34; //字面值常量

	enum Sex  
	{ 
		//枚举常量，默认从0值增加 

		male,
		female,
		secret
	};

	const int a = 23;
	// a = 34; 改变量被const修饰后变成了常量

	//define定义的标识符常量
	#define MAX 100
	printf("%d\n", MAX);

	printf("%d\n", male);
	printf("%d\n", female);
	printf("%d\n",secret);
	return 0;
}*/

//字符串 以\0为结束标志，标志，不算内容。
/*
int main() 
{
	char arr1[] = "abc";
	char arr2[] = { 'a','b','c' };
	char arr3[] = { 'a','b','c','\0' };

	printf("%s\n", arr1);
	printf("%s\n", arr2);
	printf("%s\n", arr3);

	return 0;
}*/

//转义字符 注意效果就行
/*
int main()
{
	printf("c:\c语言\test.c\n");
	// c:c语言 est.c
	// \t 水平制表符 \n换行符 \  这些是转义字符

	 //问题1：在屏幕上打印一个单引号'，怎么做？
	//问题2：在屏幕上打印一个字符串，字符串的内容是一个双引号“，怎么做？
	printf("\'\n");
	printf("\"\n");
	printf("%c", '\"');
	printf("%s", "\"");

	return 0;
}*/

//选择语句
/*
int main()
{
	printf("你会好好学习吗？\n 1.是 2.否 \n");
	int num = 0;
	scanf("%d", &num);

	if (num == 1)
	{
		printf("期待你拿到好offer！\n");
	}
	else 
	{
		printf("那只可能回家买红薯了。\n");
	}
	return 0;
}*/

//结构体 定义 和 使用
int main()
{
	struct Stu
	{
		char name[20];
		int age;
		char id[10];
	};

	struct Stu stu = { "李四",17,"111234" };
	struct Stu* ps = &stu;
	printf("%s\n%d\n%s\n", stu.name,stu.age,ps->id);

	return 0;
}