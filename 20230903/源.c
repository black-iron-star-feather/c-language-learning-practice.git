#define _CRT_SECURE_NO_WARNINGS 1
BC105 菱形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 
        int i = 0;
        int j = 0;
        int b = a;
        int p = 0;
        for (i = 0; i <= a; i++)
        {
            for (j = b; j > 0; j--)
            {
                printf(" ");
            }
            b--;
            printf("* ");
            for (p = 1; p <= i; p++)
            {
                printf("* ");
            }
            printf("\n");
        }
        //打印下半部分
        b = a;
        for (i = 1; i <= a; i++)
        {
            for (j = 1; j <= i; j++)
            {
                printf(" ");
            }
            for (p = b; p > 0; p--)
            {
                printf("* ");
            }
            b--;
            printf("\n");
        }

    }
    return 0;
}