#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

void RET(char *p,int i)
{
	char* left = p;
	char* right = (p+i);
	char tmp = 0;
	while (left < right)
	{
		tmp = *left;
		*left = *right;
		*right = tmp;
		left++;
		right--;
	}
}

//void PRINT(char* p)
//{
//	while(*p!='\0')
//	{
//		printf("%c", *p++);
//	}
//}

int main()
{
	char ch[100];
	int i = 0;
	while (scanf("%c", &ch[i]), i++, ch[i - 1] != '\n')
	{
		;
	}
	ch[i - 1] = '\0';
	RET(ch,i-2);
	//PRINT(ch);
	printf("%s", ch);
	return 0;
}
