#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>                       //多组输入 注意scanf函数的使用
int main()                                  //关于scanf函数我在博客上有详解和实例哟
{                                               //链接赋给大家↓↓↓
    int i = 0;                              //https://blog.csdn.net/zoe23333/article/details/122009836?spm=1001.2014.3001.5501
    while (scanf("%d", &i) != EOF)         
    {
        if (i < 140)
            ;
        else
            puts("Genius");             // //要让这个循环终止就按 Ctrl+d
    }

    return 0;
}
