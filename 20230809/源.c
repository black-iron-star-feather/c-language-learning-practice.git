#define _CRT_SECURE_NO_WARNINGS 1
//BC9 字符转ASCII码
//#include <stdio.h>
//
//int main()
//{
//    char ch;
//    scanf("%c", &ch);
//
//    printf("%d\n", ch);
//
//    return 0;
//}
//BC11 成绩输入输出
//#include <stdio.h>
//
//int main() {
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//    printf("score1=%d,score2=%d,score3=%d\n", a, b, c);
//    return 0;
//}
//BC12 学生基本信息输入输出
//#include <stdio.h>
//
//int main() {
//    float a, b, c;
//    long studentID;
//    scanf("%ld;%f,%f,%f", &studentID, &a, &b, &c);
//
//    printf("The each subject score of No. %ld is %.2f, %.2f, %.2f.", studentID, a, b, c);
//    return 0;
//}
//BC13 出生日期输入输出
// 方法一：快，注意scanf函数也可以这样拆分输入数据的。
//#include<stdio.h>
//int main()
//{
//    int a, b, c;
//    scanf("%4d%2d%2d", &a, &b, &c);
//    printf("year=%d\nmonth=%02d\ndate=%02d\n", a, b, c);
//}
//方法二：只需要注意printf输出时先写0若有空位就会自动补0，然后写2控制位数的。
//#include<stdio.h>
//int main()
//{
//    int year;
//    int month, date;
//    int num;
//    scanf("%d", &num);
//    date = num % 100;
//
//    month = (num / 100) % 100;
//    year = num / 10000;
//
//    printf("year=%d\nmonth=%02d\ndate=%02d\n", year, month, date);
//}
//BC14 按照格式输入并交换输出
//#include <stdio.h>
//
//int main() {
//    int a, b;
//    scanf("a=%d,b=%d", &a, &b);
//    printf("a=%d,b=%d", b, a);
//    return 0;
//}
//BC16 十六进制转十进制
//#include <stdio.h>
//
//int main() {
//    //int num=0xABCDEF;
//    printf("%15d", 0xABCDEF);
//    return 0;
//}
//BC17 缩短二进制
//"%d"表示以十进制整数形式输出
//"%o"表示以八进制形式输出
//"%x"表示以十六进制输出
//"%X"表示以十六进制输出, 并且将字母(A、B、C、D、E、F)换成大写
//"%e"表示以科学计数法输出浮点数
//"%E"表示以科学计数法输出浮点数, 而且将e大写
//"%f"表示以十进制浮点数输出, 在"%f"之间加上".n"表示输出时保留小数点后面n位
//#include <stdio.h>
//
//int main() {
//    int num = 1234;
//    printf("%#o %#X\n", num, num);
//    return 0;
//}
//BC18 牛牛的空格分隔BC18 牛牛的空格分隔
//#include <stdio.h>
//
//int main() {
//    char ch;
//    int a;
//    float bb;
//    scanf("%c%d%f", &ch, &a, &bb);
//    printf("%c %d %.6f", ch, a, bb);
//    return 0;
//}
//BC19 牛牛的对齐
//
//int main()
//{
//    int a = 0, b = 0, c = 0;
//    scanf("%d %d %d", &a, &b, &c);
//    printf("%d%8d%8d", a, b, c);
//    //printf("%-8d%-8d%d",a,b,c);也可以这样，-8d表示在第一位数字靠左然后再后面补齐7位
//    //%nd表示右对齐
//    //%-nd表示左对齐
//    return 0;
//}
//#include <stdio.h>
//
//int main() {
//    int a, b, c;
//    scanf("%d%d%d", &a, &b, &c);
//
//    printf("%-8d%-8d%-8d", a, b, c);
//
//    return 0;
//}
