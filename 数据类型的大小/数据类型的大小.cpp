#include <stdio.h>			//注意sizeof是单目操作符，用来计算类型大小的。
int main()					//可以像下面这样直接输出使用，单位：字节。
{
	printf("%d\n", sizeof(char));
	printf("%d\n", sizeof(short));
	printf("%d\n", sizeof(int));
	printf("%d\n", sizeof(long));
	printf("%d\n", sizeof(long long));
	printf("%d\n", sizeof(float));
	printf("%d\n", sizeof(double));
	return 0;
}