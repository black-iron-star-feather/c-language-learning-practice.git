#define _CRT_SECURE_NO_WARNINGS 1
//描述
//将一个四位数，反向输出。
//
//
//输入描述：
//一行，输入一个整数n（1000 <= n <= 9999）。
//输出描述：
//针对每组输入，反向输出对应四位数。
//示例1
//输入：
//1234
//复制
//输出：
//4321

#include <stdio.h>
int main()
{
	int n = 0;
	int a = 0;
	scanf("%d", &n);

	while (n>0)
	{
		a=n % 10;
		n = n / 10;
		printf("%d",a );
	}
	printf("\n");
	return 0;
}