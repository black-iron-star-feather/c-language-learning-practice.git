#define _CRT_SECURE_NO_WARNINGS 1
BC106 K形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 
        int b = a + 1;
        int i = 0;
        int j = b;
        for (i = 1; i <= (a + 1); i++)
        {
            for (j = b; j >= 1; j--)
            {
                printf("* ");
            }
            printf("\n");
            b--;
        }
        b = a + 1;
        for (i = 2; i <= (a + 1); i++)
        {
            for (j = 1; j <= i; j++)
            {
                printf("* ");
            }
            printf("\n");

        }

    }
    return 0;
}
BC108 反斜线形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 

        int i = 0;
        int j = 0;

        for (i = 1; i <= a; i++)
        {
            for (j = 1; j < i; j++)
            {
                printf(" ");
            }
            printf("*\n");

        }

    }
    return 0;
}
BC109 正斜线形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 

        int i = 0;
        int j = 0;

        for (i = a; i >= 1; i--)
        {
            for (j = i; j > 1; j--)
            {
                printf(" ");
            }
            printf("*\n");

        }

    }
    return 0;
}
