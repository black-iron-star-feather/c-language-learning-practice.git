#define _CRT_SECURE_NO_WARNINGS 1
BC111 空心正方形图案
#include <stdio.h>

int main() {
    int a;
    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
        // 64 位输出请用 printf("%lld") to 
        int i = 0;
        int j = 0;
        for (i = 1; i <= a; i++)
        {
            printf("* ");
        }
        printf("\n");
        for (i = 2; i < a; i++)
        {
            printf("* ");
            for (j = 2; j < a; j++)
            {
                printf("  ");
            }
            printf("* \n");
        }
        for (i = 1; i <= a; i++)
        {
            printf("* ");
        }
        printf("\n");

    }
    return 0;
}