#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
int main()
{
    int x = 0, y = 0;
    scanf("%d", &x);        //if语句条件为真则执行
    if (x < 0)
        y = 1;          //注意if语句的嵌套
    else
    {                   //为了方便大家，在写的时候尽量把大括号都写出来，
        if (x == 0)     //这样便于区分嵌套结构中的每一层。
            y = 0;
        else y = -1;
    }
    printf("%d\n", y);
    return 0;
}