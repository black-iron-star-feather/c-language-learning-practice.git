#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//实现字母的大小写转换。多组输入输出。
//多组输入过程中要注意“回车”也是字母，所以要“吸收”(getchar())掉该字母。
int main()
{
    char i = 0;
    while (scanf("%c", &i) != EOF)
    {
        if(i>='A' && i<='Z')
        i += 32;    //注意这里应该对是否是正确的结果进行判断！！

        printf("%c\n", i);
        getchar();  //处理缓冲区的问题其实一直存在，所以得重视
    }

    return 0;
}